:- dynamic xpozytywne/2.
:- dynamic xnegatywne/2.
:- dynamic asked/2.
:- style_check(-discontiguous).

:-(clause(samochody(_,_,_),_); consult('samochody_bw.pl')).
pozytywne(X,Y):-
    xpozytywne(X,Y),
    !.

pozytywne(X,Y):-
    not(xnegatywne(X,Y)),
    pytaj(X,Y,tak).

negatywne(X,Y):-
    xnegatywne(X,Y),
    !.

negatywne(X,Y):-
    not(xpozytywne(X,Y)),
    pytaj(X,Y,nie).


pytaj(X,Y,tak):-
    !,
    not(asked(X,Y)),
    assertz(asked(X,Y)),
    write(X),write(' ten_samochod '),write(Y),write(' ?(t/n)'),nl,
    read(Odp),
    Odp = t,
    assertz(xpozytywne(X,Y)).

pytaj(X,Y,nie):-
    !,
    not(asked(X,Y)),
    assertz(asked(X,Y)),
    write(X),write(' ten_samochod '),write(Y),write(' ?(t/n)'),nl,
    read(Odp),
    Odp = n,
    assertz(xnegatywne(X,Y)).

wyczysc_fakty:-
    retractall(xpozytywne(_,_)),
    retractall(xnegatywne(_,_)),
    retractall(asked(_,_)),
    write('Fakty usuniete'),nl.

co_to_za_samochod(X) :- samochod(X).

menu:-
        consult('samochody_bw.pl'),
        write('**************************************************'),nl,
        write('*                                                 *'),nl,
        write('*                SAMOCHODY                        *'),nl,
        write('*                                                 *'),nl,
        write('***************************************************'),nl,
        write('*    1. - ZGADNIJ SAMOCHOD                        *'),nl,
        write('*    2. - DODAWANIE SAMOCHODU                     *'),nl,
        write('*    3. - KONIEC PRACY                            *'),nl,
        write('***************************************************'),nl,
        nl,nl,
        write('Wybor (zakonczony kropka): '),
        read(X), 
        menu(X).

menu(1):-
    nl,nl,
    write('Sprobuje zgadnac o jaki samochod chodzi!'),nl,
    co_to_za_samochod(X),
    nl,nl,
    write('Ten samochod to: '),
    write(X),
    nl,nl,
    write('Jeszcze raz? (t/n)'),nl,
    read(Odp),
    (Odp = t, wyczysc_fakty, menu(1) ; !, wyczysc_fakty ,nl, menu).

menu(1):-
    nl,nl,
    write('Niestety nie moge odgadnac...'),nl,
    write('Jeszcze raz? (t/n)'),nl,
    read(Odp),
    (Odp = t, wyczysc_fakty, menu(1) ; !, wyczysc_fakty, nl, menu).

menu(2):-
            nl,nl,
	        write('Podaj model samochodu(zakonczone kropka): '), nl,
	        read(Model),
            write('Podaj typ samochodu (sedan, coupe, kombi, suv, tir)'), nl,
            read(Typ),
            write('Podaj kraj pochodzenia samochodu:'), nl,
            read(Kraj),
            write('Podaj marke(zakonczona kropka): '),nl,
            read(Nazwa),
            write('Podaj rok produkcji'), nl,
            read(RokProdukcji),
            (samochod(Model), write('taki samochod juz istnieje'), nl ;
            open('samochody_bw.pl', append, HANDLER),
            current_output(PREV),
            set_output(HANDLER),
            nl, write('samochod('), write(Model), write(') :-'), nl,
            write('    jest_to('), write(Model), write(').'), nl, nl,
            write('jest_to('), write(Model), write(') :-'), nl,
	        write('    pozytywne(czy,jest_z_'), write(Kraj), write(')'), write(','), nl,
            write('    pozytywne(czy,to_'), write(Nazwa), write(')'), write(','), nl, 
            write('    pozytywne(czy,to_'), write(Typ), write(')'), write(','), nl, 
            write('    pozytywne(czy,jest_z_'), write(RokProdukcji), write(')'), write('.'), nl,
            set_output(PREV),
            close(HANDLER)),
            
            dalej(Odp),
            (Odp = t, menu(2) ; menu).
menu(3):-        
            nl,
            write('koniec pracy...'),
            nl,
            halt.

menu(_):-    
            nl,nl,
            write('bledny numer!'),
            nl,
            menu.
dalej(Odp):-
            nl,
            write('kontynuacja? t/n'),nl,
            read(Odp).