:- dynamic samochod/1.
:- dynamic jest_to/1.
:- style_check(-discontiguous).


jest_to(fabia):-
    pozytywne(czy,jest_z_czech),
    pozytywne(czy,to_Skoda),
    pozytywne(czy,to_coupe),
    pozytywne(czy,jest_z_1999).

jest_to(kuga):-
    pozytywne(czy,jest_z_usa),
    pozytywne(czy,to_ford),
    pozytywne(czy,to_suv),
    pozytywne(czy,jest_z_2008).

jest_to(mercedes_actros):-
    pozytywne(czy,jest_z_niemiec),
    pozytywne(czy,to_mercedes),
    pozytywne(czy,to_tir),
    pozytywne(czy,jest_z_2007).

jest_to(toyota_avensis):-
    pozytywne(czy,jest_z_japonii),
    pozytywne(czy,to_toyota),
    pozytywne(czy,to_sedan),
    pozytywne(czy,jest_z_1997).

jest_to(bmw_i8):-
    pozytywne(czy,jest_z_niemiec),
    pozytywne(czy,to_bmw),
    pozytywne(czy,to_coupe),
    pozytywne(czy,jest_z_2014).

jest_to(peugeot_407):-
    pozytywne(czy,jest_z_francji),
    pozytywne(czy,to_peugeot),
    pozytywne(czy,to_sedan),
    pozytywne(czy,jest_z_2004).

jest_to(ferrari_portofino):-
    pozytywne(czy,jest_z_wloch),
    pozytywne(czy,to_ferrari),
    pozytywne(czy,to_coupe),
    pozytywne(czy,jest_z_2017).

jest_to(opel_astra):-
    pozytywne(czy,jest_z_niemiec),
    pozytywne(czy,to_opel),
    pozytywne(czy,to_coupe),
    pozytywne(czy,jest_z_1991).
    
jest_to(ford_gt):-
    pozytywne(czy,jest_z_usa),
    pozytywne(czy,to_ford),
    pozytywne(czy,to_coupe),
    pozytywne(czy,jest_z_2003).
   
jest_to(renault_megane):-
    pozytywne(czy,jest_z_francji),
    pozytywne(czy,to_renault),
    pozytywne(czy,to_sendan),
    pozytywne(czy,jest_z_2001).

jest_to(renault_koleos):-
    pozytywne(czy,jest_z_francji),
    pozytywne(czy,to_renault),
    pozytywne(czy,to_suv),
    pozytywne(czy,jest_z_2005).

jest_to(lamborghini_urus):-
    pozytywne(czy,jest_z_wloch),
    pozytywne(czy,to_lamborghini),
    pozytywne(czy,to_suv),
    pozytywne(czy,jest_z_2017).  

jest_to(lamborghini_diablo):-
    pozytywne(czy,jest_z_wloch),
    pozytywne(czy,to_lamborghini),
    pozytywne(czy,to_coupe),
    pozytywne(czy,jest_z_1990).  

samochod(bmw_i8):-
    jest_to(bmw_i8).

samochod(peugeot_407):-
    jest_to(peugeot_407).

samochod(fabia):-
    jest_to(fabia).

samochod(kuga):-
    jest_to(kuga).

samochod(actros):-
    jest_to(actros).

samochod(avensis):-
    jest_to(avensis).

samochod(i8):-
    jest_to(i8).

samochod(ferrari_portofino):-
    jest_to(ferrari_portofino).

samochod(opel_astra):-
    jest_to(opel_astra).

samochod(ford_gt):-
    jest_to(ford_gt).

samochod(renault_megane):-
    jest_to(renault_megane).

samochod(renault_koleos):-
    jest_to(renault_koleos).

samochod(lamborghini_urus):-
    jest_to(lamborghini_urus).

samochod(lamborghini_diablo):-
    jest_to(lamborghini_diablo).
